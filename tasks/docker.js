/**
 * Module with the grunt-foobar plugin
 *
 * @module grunt-docker-images
 * @author Basalt AB
 * @license
 * Copyright (c) 2018 Basalt AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

'use strict';

module.exports = function(grunt) {
  var Docker = require('./lib/docker');

  grunt.registerMultiTask('docker', 'Manage container images.', function(action = 'build') {

    const extend = require('extend');
    const execSync = require('child_process').execSync;

    var defaults = {
      registry: 'docker.io',
      project: process.env.USER,
      folder: 'containers'
    };

    var options = extend({}, defaults, grunt.config.get('docker').options);

    var docker = new Docker(this.target, this.data, options);
    var cmds = [];

    switch (action) {
      case 'build':
        cmds = docker.build();
        break;
      case 'test':
        cmds = docker.test();
        break;
      default:
        grunt.log.warn('Unknown action: ' + action);
    }

    for (let cmd of cmds) {
      execSync(cmd, {stdio:[0,1,2]});
    }
  });
};
