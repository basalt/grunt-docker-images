/**
 * Module for managing container images
 *
 * @module Docker
 * @author Basalt AB
 * @license
 * Copyright (c) 2018 Basalt AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

'use strict';

/**
 * Wraps the Docker CLI tool
 */
class Docker {

  constructor(name, values = {}, options = {}) {
    this.name = name;
    this.version = values.version || 'latest';
    this.registry = values.registry || options.registry;
    this.project = values.project || options.project;
    this.buildargs = values;
    this.path = values.path || `${options.folder}/${name}`;

    if (typeof(values.testPath) !== 'undefined') {
      this.testPath = values.testPath;
    } else if (typeof(options.testFolder) !== 'undefined') {
      this.testPath = `${options.testFolder}/${name}`;
    } else {
      this.testPath = `test/${options.folder}/${name}`;
    }

  }

  /**
   * Build a container image
   */
  build() {
    let image = `${this.registry}/${this.project}/${this.name}:${this.version}`;
    let args = [
      [ 'tag', image ]
    ];

    if (typeof(this.buildargs) !== undefined) {
      for (const k of Object.keys(this.buildargs)) {
        args.push(['build-arg', `'${k}=${this.buildargs[k]}'`]);
      }
    }

    let flags = args.map(k => `--${k[0]} ${k[1]}`).join(' ');
    let cmd = `docker build ${flags} ${this.path}`;
    return [cmd];
  }

  /**
   * Test a container image
   */
  test() {
    var cmds = [];
    let image = `${this.registry}/${this.project}/${this.name}`;
    let args = [
      [ 'tag', `${image}:test` ],
      [ 'build-arg', `image_path=${image}:${this.version}`]
    ];

    if (typeof(this.buildargs) !== undefined) {
      for (const k of Object.keys(this.buildargs)) {
        args.push(['build-arg', `'${k}=${this.buildargs[k]}'`]);
      }
    }

    let flags = args.map(k => `--${k[0]} ${k[1]}`).join(' ');
    cmds.push(`docker build ${flags} ${this.testPath}`);
    cmds.push(`docker run --rm ${image}:test`);
    return cmds;
  }
}

module.exports = Docker;
