# Change log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.2.0] - 2018-11-21
### Added
- Images can be tested by running `grunt docker:my_image:test`.

## [0.1.2] - 2018-11-20
### Fixed
- It was not possible to have whitespace in image meta data.

## [0.1.1] - 2018-11-20
### Fixed
- The build command was printed instead of executed.

## [0.1.0] - 2018-11-20
### Added
- Grunt plugin for building container images using Docker.
- The project was created with `yo basalt:npm`.

[0.2.0]: https://gitlab.com/basalt/grunt-docker-images/compare/0.1.2...0.2.0
[0.1.2]: https://gitlab.com/basalt/grunt-docker-images/compare/0.1.1...0.1.2
[0.1.1]: https://gitlab.com/basalt/grunt-docker-images/compare/0.1.0...0.1.1
[0.1.0]: https://gitlab.com/basalt/grunt-docker-images/compare/8954f10...0.1.0
