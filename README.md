# grunt-docker-images

Grunt plugin for managing container images with Docker.

## Getting Started

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out
the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains
how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as
install and use Grunt plugins. Once you're familiar with that process, you may
install this plugin with this command:

```shell
npm install grunt-docker-images --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with
this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-docker-images');
```

## Docker task

_Run this task with the `grunt docker` command._

Task targets, files and options may be specified according to the grunt
[Configuring tasks](http://gruntjs.com/configuring-tasks) guide.

### Global options

#### options.registry

Type: `String`. Default value `docker.io`.

The name of the registry to use. For example `registry.example.com` would build
the image `registry.example.com/PROJECT_NAME/IMAGE_NAME:VERSION`.

#### options.project

Type: `String`. Default value is the environment variable `USER`.

The name of the project in the registry to use. For example `acme` would build
the image `docker.io/acme/IMAGE_NAME:VERSION`.

#### options.folder

Type: `String`. Default value `containers`.

The folder where container sources are located. For example by setting the
value to `docker`, the image `my_app` would build the container sourcces inside
`docker/my_app`.

#### options.testFolder

Type: `String`. Default value `test/containers`.

The folder where container images for testing is located.

### Image options

#### options.version

Type: `String`. Default value `latest`.

The tag to build. For example by setting the value to `1.0.0`, the image
`docker.io/PROJECT/IMAGE:1.0.0` would be built.

#### options.path

Type: `String`.

The folder that should be built when building the container image. If not set
the folder is assumed to be named the same as the image inside the folder
specified by `options.folder`.

#### options.testPath

Type: `String`.

The folder where the test container is located. If not set the folder is assumed
to be named the same as the image inside the folder specified by
`options.testFolder`.

#### Build arguments

All options for the image are also sent as build arguments. For example, the
version number can be used as such:

```dockerfile
FROM alpine
ARG version
RUN apk add --no-cache mypackage=$version
```

## Example

Each target should correspond to a playbook. The options under each target
is turned into command line arguments. For example:

```js
docker: {
  options: {
    folder: 'containers',
    registry: 'registry.example.com',
    project: 'my_project'
  },
  nginx: {
    version: 'latest',
    title: 'Nginx',
    vendor: 'Nginx, Inc.',
    license: 'BSD 2-clause'
  }
}
```

The images can then be built with:

```shell
$ grunt docker:nginx:build
...
```

### Testing container images

Images can also be tested by runing:

```shell
$ grunt docker:nginx:test
...
```

Testing is done by placing a second container inside
a test folder. By default this is `test/containers`. When the test container
is built it will be injected with the argument `image_name` which can be used
to base the test image on the origin image like so:

```dockerfile
ARG image_name
FROM $image_name

RUN install_test_tools

CMD run_test_command
```

After the test image is built it will be run.
