'use strict';

describe("Docker", function() {
  var Docker = require('../tasks/lib/docker.js');
  var options = {
    registry: 'docker.io',
    project: 'proj',
    folder: 'containers'
  };

  describe(".build", function() {

    it("should build container", function() {
      var docker = new Docker('test', {}, options);
      expect(docker.build()).toEqual([
        'docker build --tag docker.io/proj/test:latest containers/test'
      ]);
    });

    it("should send build args", function() {
      let img = {
        'version': '1.0.0',
        'title': 'My fancy title'
      }
      var docker = new Docker('test', img, options);
      expect(docker.build()).toEqual([
        'docker build --tag docker.io/proj/test:1.0.0 ' +
          "--build-arg 'version=1.0.0' " +
          "--build-arg 'title=My fancy title' containers/test"
      ]);
    });

  });

  describe(".test", function() {

    it("should test a container", function() {
      let img = {
        'version': '1.0.0',
        'title': 'My fancy title'
      }
      var docker = new Docker('foo', img, options);
      expect(docker.test()).toEqual(
        [
          'docker build --tag docker.io/proj/foo:test ' +
            '--build-arg image_path=docker.io/proj/foo:1.0.0 ' +
            "--build-arg 'version=1.0.0' " +
            "--build-arg 'title=My fancy title' " +
            'test/containers/foo',
          'docker run --rm docker.io/proj/foo:test'
        ]
      );
    });

    it("should use test folder", function() {
      var docker = new Docker('foo', {}, { testFolder: 'specs' });
      expect(docker.test()[0]).toContain(' specs/foo');
    });

    it("should use test path", function() {
      let options = { testFolder: 'specs' };
      var docker = new Docker('foo', { testPath: 'foo_test/container'}, {});
      expect(docker.test()[0]).toContain(' foo_test/container');
    });

  });
});
